package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.Employee.EmployeeRequest;
import com.afs.restapi.service.dto.Employee.EmployeeResponse;
import com.afs.restapi.service.mapper.employee.EmployeeMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final JPAEmployeeRepository jpaEmployeeRepository;

    public EmployeeService(JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public List<Employee> findAll() {
        return jpaEmployeeRepository.findAll();
    }

    public EmployeeResponse findById(Long id) {
        Employee employee = jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        return EmployeeMapper.toResponse(employee);
    }

    public void update(Long id, EmployeeRequest employeeRequest) {
        EmployeeResponse employeeResponse = findById(id);
        Employee toBeUpdatedEmployee = EmployeeMapper.toEntiry(employeeResponse);

        if (employeeRequest.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employeeRequest.getSalary());
        }
        if (employeeRequest.getAge() != null) {
            toBeUpdatedEmployee.setAge(employeeRequest.getAge());
        }
        jpaEmployeeRepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return jpaEmployeeRepository.findByGender(gender);
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntiry(employeeRequest);
        Employee saveEmployee = jpaEmployeeRepository.save(employee);

        return EmployeeMapper.toResponse(saveEmployee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        Page<Employee> employees = jpaEmployeeRepository.findAll(pageRequest);
        return employees.get().collect(Collectors.toList());
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
