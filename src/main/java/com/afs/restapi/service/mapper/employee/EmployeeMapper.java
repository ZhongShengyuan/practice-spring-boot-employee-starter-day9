package com.afs.restapi.service.mapper.employee;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.Employee.EmployeeRequest;
import com.afs.restapi.service.dto.Employee.EmployeeResponse;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {

    public static Employee toEntiry(EmployeeRequest employeeRequest) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeRequest, employee);
        return employee;
    }

    public static Employee toEntiry(EmployeeResponse employeeResponse) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeResponse, employee);
        return employee;
    }

    public static EmployeeResponse toResponse(Employee employee) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee, employeeResponse);
        return employeeResponse;
    }
}
