package com.afs.restapi.service.mapper.company;

import com.afs.restapi.entity.Company;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.Company.CompanyRequest;
import com.afs.restapi.service.dto.Company.CompanyResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class CompanyMapper {

    @Autowired
    private static JPAEmployeeRepository jpaEmployeeRepository;

    public static Company toEntiry(CompanyRequest companyRequest){
        Company company = new Company();
        BeanUtils.copyProperties(companyRequest, company);
        return company;
    }

    public static CompanyResponse toCompanResponse(Company company, int employeeNumber){
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company, companyResponse);
        companyResponse.setEmployeesCount(employeeNumber);
        return  companyResponse;
    }
}
