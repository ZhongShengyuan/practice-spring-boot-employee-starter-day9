package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.Company.CompanyRequest;
import com.afs.restapi.service.dto.Company.CompanyResponse;
import com.afs.restapi.service.dto.Employee.EmployeeResponse;
import com.afs.restapi.service.mapper.company.CompanyMapper;
import com.afs.restapi.service.mapper.employee.EmployeeMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    private final JPACompanyRepository jpaCompanyRepository;
    private final JPAEmployeeRepository jpaEmployeeRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository, JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }


    public List<Company> findAll() {
        return jpaCompanyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        Page<Company> companies = jpaCompanyRepository.findAll(pageRequest);
        return companies.get().collect(Collectors.toList());
    }

    public CompanyResponse findById(Long id) {
        Company company = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        int employeeNumber = company.getEmployees().size();
        CompanyResponse companyResponse = CompanyMapper.toCompanResponse(company, employeeNumber);
        return companyResponse;

    }

    public void update(Long id, CompanyRequest companyRequest) {
        Company findCompany = jpaCompanyRepository.findById(id).get();
        findCompany.setName(companyRequest.getName());
        jpaCompanyRepository.save(findCompany);
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = CompanyMapper.toEntiry(companyRequest);
        return CompanyMapper.toCompanResponse(jpaCompanyRepository.save(company), 0);
    }

    public List<EmployeeResponse> findEmployeesByCompanyId(Long id) {
        List<Employee> employees = jpaCompanyRepository.findById(id)
                .map(Company::getEmployees)
                .orElseThrow(CompanyNotFoundException::new);

        return employees.stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());

    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }

}
